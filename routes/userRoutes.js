const express = require("express");
const router = express.Router();

// In fact, routes should just contain the endpoints and it should only trigger the function but it should not be where we define the logic of our function.

// The business logic of our API should be in controllers.
// import our user controllers:
const userControllers = require("../controllers/userControllers");
// check the imported userControllers:
// console.log(userControllers);

// import auth to be able to have access and use the verify methods to act as a middleware for our routes.
// Middlewares add in the route such as verify() will have access to the req,res objects.
const auth = require("../auth");

// destructure auth to get only our methods and save it in variables:
const { verify } = auth;

/*
	Updated Route Syntax:

	method("/endpoint",handlerFunction)
*/

// register
router.post('/',userControllers.registerUser);

// POST method route to get user details by id:
// http://localhost:4000/users/detailes

// verify() is used as a middleware which means our request will get through verify first before our controller

// verify() will not only check the validity of the token but also add the decoded data of the token in the request object as req.user

router.get('/details',verify,userControllers.getUserDetails);

// Router for User Authentication
router.post('/login',userControllers.loginUser);

// Activity - checkEmail then show user data
router.post('/checkEmail',userControllers.checkEmail)

// User Enrollment
// courseId will come from the req.body
// userId will come from the req.user
router.post('/enroll',verify,userControllers.enroll);


module.exports = router;